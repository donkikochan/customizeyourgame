﻿using UnityEngine;
using System.Collections;



public class FP_ItemManager : MonoBehaviour {

	public enum FP_IM_Type
	{
		Rain, 
		Gravity, 
		Speed,
		None
	};
	
	public enum FP_AppearType
	{
		EveryXSeconds,
		EveryXPipes,
		JustOnceAtXSeconds,
		JustOnceAtXPipes,
		None
	};

	public	FP_AppearType	appearType;
	[HideInInspector]
	public	FP_IM_Type		itemType;

	//---Public:
	public	float		timeToStart;
	public	float		timeToRepeat;
	public	int			pipeToStart;
	public	int			pipesToRepeat;

	//---Private:
	private float			timer;
	private FP_Generator	generator;
	private bool			firstTime;
	private int				numPipes;


	void Awake ()
	{
		if (GetComponent<FP_GravityManager> () != null) 
		{
			itemType = FP_IM_Type.Gravity;
		}
		else if (GetComponent<FP_RainManager> () != null) 
		{
			itemType = FP_IM_Type.Rain;
		}
		else if (GetComponent<FP_SpeedManager> () != null) 
		{
			itemType = FP_IM_Type.Speed;
		}
		else 
		{
			itemType = FP_IM_Type.None;
		}
	}
	// Use this for initialization
	void Start () {
		generator = GameObject.FindGameObjectWithTag ("FP_Generator").GetComponent<FP_Generator> ();
		timer = 0.0f;
		firstTime = false;
	}
	
	// Update is called once per frame
	void Update () 
	{
		timer += Time.deltaTime;

		switch (appearType)
		{
		case FP_AppearType.EveryXPipes:
			if (!firstTime)
			{
				if (generator.numPipes == pipeToStart)
				{
					CreateItem();
					firstTime = true;
				}
			}
			else
			{
				int pipes = generator.numPipes - pipeToStart;
				if (generator.numPipes != numPipes && pipes!=0 && pipes%pipesToRepeat==0)
				{
					CreateItem();
				}
			}
			break;
		case FP_AppearType.EveryXSeconds:
			if (!firstTime)
			{
				if (timer > timeToStart)
				{
					CreateItem();
					firstTime = true;
					timer = 0.0f;
				}
			}else if (timer > timeToRepeat)
			{
				CreateItem();
				timer = 0.0f;
			}
			break;

		case FP_AppearType.JustOnceAtXPipes:
			if (generator.numPipes == pipeToStart && !firstTime)
			{
				CreateItem();
				firstTime = true;
			}
			break;

		case FP_AppearType.JustOnceAtXSeconds:
			if (timer > timeToRepeat && !firstTime)
			{
				CreateItem();
				firstTime = true;
			}
			break;
		}

		numPipes = generator.numPipes;
	}

	public void CreateItem ()
	{
		generator.CreateItem (itemType);
	}
	
}


