﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UI_Script : MonoBehaviour {

	public Text BestScore_FlappyBird;
	public Text BestScore_Memmory;
	public Text BestScore_Connect4;
	public Text BestScore_TicTacToe;
	public Text BestScore_WhackAttack;

	MainMenu mainMenu;


	// Use this for initialization
	void Start () 
	{
		GameObject oMainMenu = GameObject.Find ("MainMenu");
		if (oMainMenu != null) 
		{
			mainMenu = oMainMenu.GetComponent<MainMenu> ();
			float 	fBestScore = -1.0f;
			int 	iBestScore = -1;

			//---Score for Memmory Game----
			fBestScore = mainMenu.GetScore(MainMenu.Game.Memmory);
			if (fBestScore > 0.0f)
			{
				BestScore_Memmory.text = "(Best Score: "+fBestScore.ToString("0.00")+")";
			}
			else
			{
				BestScore_Memmory.text = "";
			}

			//---Score for FlappyBird Game----
			iBestScore = (int)mainMenu.GetScore(MainMenu.Game.FlappyBird);
			if (iBestScore > 0)
			{
				BestScore_FlappyBird.text = "(Best Score: "+iBestScore+")";
			}
			else
			{
				BestScore_FlappyBird.text = "";
			}

			//---Score for WhackAttack Game----
			iBestScore = (int)mainMenu.GetScore(MainMenu.Game.WhactAtack);
			if (iBestScore > 0)
			{
				BestScore_WhackAttack.text = "(Best Score: "+iBestScore+")";
			}
			else
			{
				BestScore_WhackAttack.text = "";
			}


			//---Score for Connect4 Game----
			fBestScore = (int)mainMenu.GetScore(MainMenu.Game.Connect4);
			if (fBestScore > 0)
			{
				BestScore_Connect4.text = "(Best Score: "+fBestScore.ToString("0.00")+")";
			}
			else
			{
				BestScore_Connect4.text = "";
			}

			//---Score for TicTactoe Game----
			fBestScore = (int)mainMenu.GetScore(MainMenu.Game.TicTacToe);
			if (fBestScore > 0)
			{
				BestScore_TicTacToe.text = "(Best Score: "+fBestScore.ToString("0.00")+")";
			}
			else
			{
				BestScore_TicTacToe.text = "";
			}
		} 
		else 
		{
			Debug.LogWarning("Error_UI_Script::Start --> No se ha encontrado el componente MainMenu");
		}


	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}
}
