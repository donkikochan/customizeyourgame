﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class FP_Score : MonoBehaviour {

	public 	Text 	txtScore;
	public	int		score;

	// Use this for initialization
	void Start () 
	{ 
		score = 0;
		txtScore.text = "Score: " + score;
	}
	
	// Update is called once per frame
	void Update () 
	{

	}

	public void AddScore ()
	{
		score++;
		txtScore.text = "Score: " + score;
		GetComponent<AudioSource> ().Play ();
	}

	public void ExitGame ()
	{

		Application.LoadLevel (0);
	}
}
