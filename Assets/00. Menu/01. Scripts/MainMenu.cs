﻿using UnityEngine;
using System.Collections;


public class MainMenu : MonoBehaviour {

	public const string Game_FlappyBird		= "GammeFlappyBird";
	public const string Game_Memmory		= "GameMemmory";
	public const string Game_Connect4 		= "GameConnect4";
	public const string Game_WhackAttack	= "GameWhackAttack";
	public const string Game_TicTacToe		= "GameTicTacToe";

	public enum Game {FlappyBird, Memmory, Connect4, WhactAtack, TicTacToe};


	private static MainMenu _instance;


	void Awake()
	{
		DontDestroyOnLoad(transform.gameObject);
		
		if(_instance == null)
		{
			//If I am the first instance, make me the Singleton
			_instance = this;
			DontDestroyOnLoad(this);
		}
		else
		{
			//If a Singleton already exists and you find
			//another reference in scene, destroy it!
			if(this != _instance)
				Destroy(this.gameObject);
		}
	}


	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public float GetScore (Game game)
	{
		float score = -1.0f;
		switch (game) 
		{
		case Game.Connect4:
			score = PlayerPrefs.GetFloat(Game_Connect4,score);
			break;
		case Game.FlappyBird:
			score = PlayerPrefs.GetFloat(Game_FlappyBird,score);
			break;
		case Game.Memmory:
			score = PlayerPrefs.GetFloat(Game_Memmory,score);
			break;
		case Game.TicTacToe:
			score = PlayerPrefs.GetFloat(Game_TicTacToe,score);
			break;
		case Game.WhactAtack:
			score = PlayerPrefs.GetFloat(Game_WhackAttack,score);
			break;
		default:
			Debug.LogWarning("Error_MainMenu::GetScore --> Invalid value for game");
			break;
		}

		return score;
	}

	public void SetScore (Game game, float score)
	{
		switch (game) 
		{
		case Game.Connect4:
			PlayerPrefs.SetFloat(Game_Connect4,score);
			break;
		case Game.FlappyBird:
			PlayerPrefs.SetFloat(Game_FlappyBird,score);
			break;
		case Game.Memmory:
			PlayerPrefs.SetFloat(Game_Memmory,score);
			break;
		case Game.TicTacToe:
			PlayerPrefs.SetFloat(Game_TicTacToe,score);
			break;
		case Game.WhactAtack:
			PlayerPrefs.SetFloat(Game_WhackAttack,score);
			break;
		default:
			Debug.LogWarning("Error_MainMenu::SetCore --> Invalid value for game");
			break;
		}
	}

	public void GoToLevel (int index)
	{
		Application.LoadLevel (index);
	}

	public void ExitGame ()
	{
		#if UNITY_EDITOR
		UnityEditor.EditorApplication.isPlaying = false;
		#else
		Application.Quit ();
		#endif

	}
}
