﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CP_Face : MonoBehaviour {

	public enum CP_TypeFaces
	{
		Normal, 
		Lose, 
		Win,
		Power,
		None
	};

	public GameObject			face_background;
	public GameObject			face_normal;
	public GameObject			face_lose;
	public GameObject			face_win;
	public GameObject			face_power;
	public CP_TypeFaces			type;
	private List<GameObject>	faces = new List<GameObject>();
	private float				timerLose;


	// Use this for initialization
	void Start () 
	{
		if (face_normal != null) {
			faces.Add (face_normal);
		}

		if (face_lose != null) {
			faces.Add (face_lose);
		}

		if (face_win != null) {
			faces.Add (face_win);
		}

		if (face_power != null) {
			faces.Add (face_power);
		}

	}
	
	// Update is called once per frame
	void Update () 
	{
	
		if (type == CP_TypeFaces.Normal) {
			face_background.transform.Rotate (new Vector3 (0.0f, 0.0f, -Time.deltaTime * 10.0f));
		}
		if (type == CP_TypeFaces.Lose) {
			timerLose += Time.deltaTime;
			if (timerLose > 0.5f)
			{
				//timerLose = 0.0f;
				//face_normal.SetActive(!face_normal.activeSelf);
				//face_lose.SetActive(!face_lose.activeSelf);
			}
		}
	}


	public void SetFace (CP_TypeFaces _type)
	{
		type = _type;
		switch (_type)
		{
		case CP_TypeFaces.None:
			GetComponent<Animator>().SetInteger("state",2);	
			hideAllFaces();
			face_normal.SetActive(true);
			break;
		case CP_TypeFaces.Normal:
			GetComponent<Animator>().SetInteger("state",1);	
			hideAllFaces();
			face_normal.SetActive(true);
			break;
		case CP_TypeFaces.Lose:
			timerLose = 0.0f;
			GetComponent<Animator>().SetInteger("state",4);
			hideAllFaces();
			face_lose.SetActive(true);
			break;
		case CP_TypeFaces.Power:
			hideAllFaces();
			face_power.SetActive(true);
			break;
		case CP_TypeFaces.Win:
			GetComponent<Animator>().SetInteger("state",3);
			hideAllFaces();
			face_win.SetActive(true);
			break;
		default:
			hideAllFaces();
			Debug.LogWarning("Faces::SetFaces unknown type");
			break;
		}
	}
	void showAllFaces()
	{
		foreach (GameObject o in faces) 
		{
			o.SetActive(true);
		}
	}

	void hideAllFaces ()
	{
		foreach (GameObject o in faces) 
		{
			o.SetActive(false);
		}
	}

}
