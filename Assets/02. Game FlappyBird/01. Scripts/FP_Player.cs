﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class FP_Player : MonoBehaviour {

	public CP_Face			face;
	public GameObject		tutorial_1;
	public GameObject		tutorial_2;
	public Text				title;
	public Text				score;
	public FP_ViewGenerator	viewGenerator;
	public FP_Generator		generator;
	public AudioClip 		flap;
	public AudioClip 		hit;
	public AudioClip 		fall;
	public AudioClip 		gameOver;
	public AudioClip		loopGame;
	public float			rotation = 0.0f;
	public Vector2 			force = new Vector2(0,500);
	public bool				dead = false;
	public float			timer = 0.0f;
	public bool				start;
	public float			initPosY;
	public float			timeValue = 0.0f;
	public float			timeTutorial = 0.0f;
	public bool				modeRain;
	public bool				modeGravity;
	public bool				modeSpeed;
	public float			speedFactor = 1.0f;

	FP_Score	_score;
	MainMenu	_mainMenu;

	//---Vibrate params:----
	private float 	xVibe = 0.5f;
	private float 	yVibe = 0.5f;
	private float 	zVibe = 0.5f;
	private float	xRot = 0.05f;
	private float 	yRot = -0.05f;
	private float 	zRot = -0.05f;
	private float 	speed = 60.0f;
	private float 	diminish = 0.5f;
	private int 	numberOfShakes = 8;
	//----------------------



	// Use this for initialization
	void Start () {
		modeRain	= false;
		modeSpeed	= false;
		modeGravity	= false;

		start = false;
		GetComponent<Rigidbody2D> ().isKinematic = true;
		GetComponent<Animator> ().SetInteger ("state", 1);
		initPosY = transform.position.y;
		tutorial_1.SetActive(true);
		tutorial_2.SetActive(false);
		score.enabled = false;
		face.SetFace(CP_Face.CP_TypeFaces.None);
		GetComponent<AudioSource> ().loop = true;
		GetComponent<AudioSource>().PlayOneShot (loopGame);
	
		_score = GameObject.FindGameObjectWithTag ("FP_Score").GetComponent<FP_Score>();
		_mainMenu = GameObject.Find ("MainMenu").GetComponent<MainMenu>();
	}
	
	// Update is called once per frame
	void Update () {
	
		if (Application.isEditor && Input.GetKeyDown(KeyCode.C))
		{
			GetComponent<PolygonCollider2D>().enabled = false;
		}

		bool tap = (Input.touchCount == 1 && Input.GetTouch (0).phase == TouchPhase.Ended);

		if((tap || Input.GetKeyDown (KeyCode.Space)) && !start) 
		{
			start = true;
			score.enabled = true;
			GetComponent<Rigidbody2D> ().isKinematic = false;
			GetComponent<Animator> ().SetInteger ("state", 2);
			face.SetFace(CP_Face.CP_TypeFaces.Normal);
			generator.start = true;
			viewGenerator.start = true;
			title.enabled = false;
			tutorial_1.SetActive(false);
			tutorial_2.SetActive(false);
			GetComponent<AudioSource> ().loop = false;
			GetComponent<AudioSource>().Stop ();
		}

		if (!start) {
			timeValue += Time.deltaTime*5.0f;
			Vector2 pos = transform.position;
			pos.y = initPosY + Mathf.Sin(timeValue)*0.2f;
			transform.position = pos;
			timeTutorial += Time.deltaTime;
			if (timeTutorial > 1.5f)
			{
				timeTutorial = 0.0f;
				tutorial_1.SetActive(!tutorial_1.activeSelf);
				tutorial_2.SetActive(!tutorial_2.activeSelf);
			}

			return;
		}
		
		if((tap || Input.GetKeyDown(KeyCode.UpArrow)) && !dead) 
		{
			GetComponent<AudioSource>().PlayOneShot(flap);

			if (modeRain)
			{
				GetComponent<Rigidbody2D>().AddForce(force*0.72f);
			}
			else if (modeGravity)
			{
				GetComponent<Rigidbody2D>().AddForce(-force);
			}
			else
			{
				GetComponent<Rigidbody2D>().AddForce(force);
			}
			GetComponent<Rigidbody2D>().velocity = Vector2.zero;
			goUp();
		}

		if (GetComponent<Rigidbody2D> ().velocity.y < 0) {
			goDown();
		}

		if (dead) 
		{
			timer += Time.deltaTime;
			if (timer > 2.8f)
			{
				Application.LoadLevel("Game_FlappyBird");
			}
		}
	}

	void goUp()
	{
		if (rotation < 45) {
			rotation += 20.0f;
		}
		transform.rotation = Quaternion.Euler(new Vector3(0.0f,0.0f,rotation));
	}

	void goDown()
	{
		if (rotation > -20) {
			rotation -= 150.0f*Time.deltaTime;
		}
		transform.rotation = Quaternion.Euler(new Vector3(0.0f,0.0f,rotation));
	}

	void PlayGamOver()
	{
		GetComponent<AudioSource> ().PlayOneShot (gameOver);
	}

	void OnCollisionEnter2D(Collision2D coll)
	{
		if (!dead) 
		{
			Vibration vibration = Camera.main.GetComponent<Vibration>();
			vibration.StartShaking(new Vector3(xVibe, yVibe, zVibe), new Quaternion(xRot, yRot, zRot, 1), speed, diminish, numberOfShakes);
			generator.start = false;
			viewGenerator.start = false;
			GetComponent<Animator> ().SetInteger ("state", 3);
			dead = true;

			//Set BestScore:
			int numScore = (int) _mainMenu.GetScore(MainMenu.Game.FlappyBird);
			if (numScore < _score.score)
			{
				_mainMenu.SetScore(MainMenu.Game.FlappyBird,_score.score);
			}

			

			GetComponent<AudioSource>().PlayOneShot(hit);
			Invoke("PlayGamOver",hit.length);

			face.SetFace (CP_Face.CP_TypeFaces.Lose);

		}

		if (coll.gameObject.tag == "FP_Floor") {
			GetComponent<Rigidbody2D> ().isKinematic = true;
		}
	}


	public void SetSmileFace()
	{
		face.SetFace (CP_Face.CP_TypeFaces.Win);
		Invoke ("SetNormalFace", 0.5f);
	}


	public void SetNormalFace()
	{
		if (!dead) 
		{
			face.SetFace (CP_Face.CP_TypeFaces.Normal);
		}
	}


}
