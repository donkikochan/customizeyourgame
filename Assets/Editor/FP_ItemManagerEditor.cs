﻿using UnityEngine;
using System.Collections;
using UnityEditor;


[CustomEditor(typeof(FP_ItemManager))]
public class FP_ItemManagerEditor : Editor 
{
	public override void OnInspectorGUI()
	{	
		FP_ItemManager myTarget = (FP_ItemManager)target;

		myTarget.appearType=(FP_ItemManager.FP_AppearType)EditorGUILayout.EnumPopup("Appear Types",myTarget.appearType);

		switch (myTarget.appearType)
		{
		case FP_ItemManager.FP_AppearType.EveryXPipes:
			myTarget.pipeToStart 	= EditorGUILayout.IntField("Pipe To Start", myTarget.pipeToStart);
			myTarget.pipesToRepeat 	= EditorGUILayout.IntField("Pipes To Repeat", myTarget.pipesToRepeat);
			break;
		case FP_ItemManager.FP_AppearType.JustOnceAtXPipes:
			myTarget.pipeToStart 	= EditorGUILayout.IntField("Pipe To Start", myTarget.pipeToStart);
			break;
		case FP_ItemManager.FP_AppearType.EveryXSeconds:
			myTarget.timeToStart 	= EditorGUILayout.FloatField("Time To Start", myTarget.timeToStart);
			myTarget.timeToRepeat 	= EditorGUILayout.FloatField("Time To Repeat", myTarget.timeToRepeat);
			break;
		case FP_ItemManager.FP_AppearType.JustOnceAtXSeconds:
			myTarget.timeToStart 	= EditorGUILayout.FloatField("Time To Start", myTarget.timeToStart);
			break;
		}

		EditorGUILayout.HelpBox("This is a help box", MessageType.Info);
	}
}	